# Demo ArgoCD applications

Example ArgoCD applications that demonstrate how to deploy a Kustomization-based application.

## App template

Placeholders used:

* `SUPP` - supplier-specific prefix (e.g. `vshn`)
* `XXX` - any value

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  # Can be whatever you want
  name: XXX  

  # Must live in your "-argocdapps" namespace
  namespace: SUPP-argocdapps

  # Ensure resources are deleted with the app
  finalizers:
    - resources-finalizer.argocd.argoproj.io

spec:
  # Must be set to your AppProject
  project: SUPP

  destination:
    # Any namespace with your prefix
    namespace: SUPP-XXX
    # leave as is
    server: https://kubernetes.default.svc

  source:
    # Optional, defaults to the repository root
    path: XXX
    
    # Git URL
    repoURL: XXX

    # You can choose any Git ref here
    targetRevision: HEAD

  # Optional but recommended
  # If you do not set this, you have to manually sync apps after changes
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
```


## Demo Repository structure

### `<cluster>/`

A folder containing ArgoCD Applications for each cluster


### `<cluster>/00root.yml`

This is the "root" of the [App of Apps Pattern](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/#app-of-apps-pattern). One such app for each cluster exists.

The "root" app in turn then deploys all of our individual applications.


## Bootstrapping

Note that for each non-public repository, we first have to configure access to it via the ArgoCD WebGUI (under "Settings" -> 
"Repositories").

Using the aforementioned "App of Apps Pattern", we only need to deploy the "root" app for each cluster:

```sh
kubectl -n XXX-argocdapps apply -f ./test/00root.yml
```
